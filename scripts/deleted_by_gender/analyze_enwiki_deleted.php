#!/usr/bin/php
<?PHP

/*
echo 'SELECT log_title,log_timestamp FROM logging WHERE log_action="delete" and log_namespace=0;' | sql enwiki | gzip -c > enwiki_deleted.tab.gz
curl -o en_last_names.xml 'https://query.wikidata.org/sparql?query=SELECT%20DISTINCT%20%3FqLabel%20%7B%20%3Fq%20wdt%3AP31%20wd%3AQ101352%20.%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%2Cen%22.%20%7D%20%7D'
curl -o male_given_name.xml 'https://query.wikidata.org/sparql?query=SELECT%20DISTINCT%20%3FqLabel%20%7B%20%3Fq%20wdt%3AP31%20wd%3AQ12308941%20.%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%2Cen%22.%20%7D%20%7D'
curl -o female_given_name.xml 'https://query.wikidata.org/sparql?query=SELECT%20DISTINCT%20%3FqLabel%20%7B%20%3Fq%20wdt%3AP31%20wd%3AQ11879590%20.%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%2Cen%22.%20%7D%20%7D'
*/

require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;

$dir = '/data/project/wikidata-todo/scripts/deleted_by_gender' ;
$deleted_files_file = "{$dir}/enwiki_deleted.tab.gz" ;
$last_names_file = "{$dir}/en_last_names.xml" ;
$male_given_names_file = "{$dir}/male_given_name.xml" ;
$female_given_names_file = "{$dir}/female_given_name.xml" ;

function read_xml_literals ( $filename ) {
	$handle = @fopen($filename, "r");
	while (($row = fgets($handle)) !== false) {
		if ( !preg_match ( "/<literal xml:lang='en'>(.+?)<\/literal>/" , $row , $m ) ) continue ;
		$ret[strtolower(trim($m[1]))] = 1 ;
	}
	fclose($handle);
	return $ret ;
}

$last_names = read_xml_literals($last_names_file) ;
$male_given_names = read_xml_literals($male_given_names_file) ;
$female_given_names = read_xml_literals($female_given_names_file) ;

$counts = [ 'male' => 0 , 'female' => 0 , 'other' => 0 ] ;
$handle = @gzopen($deleted_files_file, "r");
while (($row = fgets($handle)) !== false) {
	list($title,$date) = explode ( "\t" , trim($row) ) ;
	$title = str_replace('_',' ',$title) ;
	$orig_title = $title ;
	if ( preg_match ( '/[0-9:\/\&\@\"]/' , $title ) ) continue ; // Bad chars
	if ( preg_match ( '/^\S+$/' , $title ) ) continue ; // One word
	if ( preg_match ( '/^\S+, \S+$/' , $title ) ) continue ; // Two words with comma
	if ( preg_match ( '/^(the|a|an) /i' , $title ) ) continue ; // Bad start
	if ( preg_match ( '/\b(museum|society|company|expedition|association|group|language|band|time|too|festival|church|bang|sex|collection|act|industries|corporation|campus|st\.|mt\.|inc\.)\b/i' , $title ) ) continue ; // Bad keyword
	if ( preg_match ( '/[A-Z]{2}/' , $title ) ) continue ; // More than one upper-case char
	if ( preg_match ( '/\b[a-z]/' , $title ) ) continue ; // Word starting with lower case

	$title = preg_replace ( '/^([A-Z]\.\s*)*/' , '' , $title ) ;
	$title = preg_replace ( '/\s*\(.*$/' , '' , $title ) ;
	if ( preg_match ( '/^\S+$/' , $title ) ) continue ; // One word

	$parts = explode ( ' ' , $title ) ;
	if ( count($parts) < 2 ) continue ; // Paranoia
	if ( count($parts) > 4 ) continue ; // Paranoia
	$last_name = strtolower(array_pop ( $parts )) ;
	if ( !isset($last_names[$last_name]) ) continue ;
	$first_name = strtolower(array_shift ( $parts )) ;
	if ( isset($female_given_names[$first_name]) ) $counts['female']++ ;
	else if ( isset($male_given_names[$first_name]) ) $counts['male']++ ;
	else $counts['other']++ ;
#	print "{$title}\n" ;
}
gzclose($handle);

print_r($counts) ;

?>