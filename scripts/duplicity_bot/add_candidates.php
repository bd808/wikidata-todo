#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '/data/project/wikidata-todo/public_html/php/common.php' ) ;

$max_batch = 10 ;


for ( $a = 1 ; $a <= 500 ; $a++ ) {

	$db = openToolDB ( 'duplicity_p' ) ;

	// Candidates for game
	$tmp = array() ;
	$r = rand() / getrandmax() ;
	$sql = "SELECT * FROM no_wd WHERE random>$r order by random limit 500" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'.":\n$sql\n\n");
	while($o = $result->fetch_object()){
		$o->title = preg_replace ( '/ \(.+\)$/' , '' , $o->title ) ;
		$tmp[] = $o ;
	}

	$db2 = openDB ( 'wikidata' , 'wikidata' , true ) ;
	if ( !isset($db2) or $db2 === null or !is_object($db2) ) exit ( 0 ) ;

	foreach ( $tmp AS $o ) {
		$to_add = array() ;
		$sql = "SELECT DISTINCT wbit_item_id 
FROM wbt_item_terms,wbt_term_in_lang,wbt_text_in_lang,wbt_text
WHERE wbit_term_in_lang_id=wbtl_id
AND wbtl_type_id IN (1,3)
AND wbtl_text_in_lang_id=wbxl_id
AND wbxl_text_id=wbx_id
AND wbx_text='" . $db2->real_escape_string($o->title) . "'" ;
		$sql .= " AND NOT EXISTS (SELECT * FROM wb_items_per_site WHERE ips_site_id='".$o->wiki."' AND ips_item_id=wbit_item_id LIMIT 1)" ;
		if(!$result = $db2->query($sql)) die('There was an error running the query [' . $db2->error . ']'.":\n$sql\n\n");
		while($o2 = $result->fetch_object()){
			$to_add[] = $o2 ;
		}
		
		if ( count ( $to_add ) > $max_batch ) continue ;
		
		foreach ( $to_add AS $o2 ) {
			$sql = "INSERT IGNORE INTO candidates (no_wd_id,q) VALUES ({$o->id},{$o2->wbit_item_id})" ;
			if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'.":\n$sql\n\n");
		}
	}
}

$db = openToolDB ( 'duplicity_p' ) ;
$to_del = array() ;
$sql = "select no_wd_id from candidates where checked=0 group by no_wd_id having count(*)>$max_batch" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'.":\n$sql\n\n");
while($o = $result->fetch_object()) $to_del[] = $o->no_wd_id ;

if ( count ( $to_del ) > 0 ) {
	$sql = "DELETE FROM candidates WHERE checked=0 and no_wd_id IN (" . implode(',',$to_del) . ")" ;
	$db->query($sql) ;
}

?>