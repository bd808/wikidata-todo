#!/usr/bin/php
<?PHP

include_once ( '../public_html/php/common.php' ) ;

#$props = array ( 22,25,40,7,9,26,45,1038 ) ; $query2 = 'and claim[31:5]' ; # Humans
$props = array ( 171,71,70 ) ; $query2 = '' ; # Taxa

$props = implode ( "," , $props ) ;

$url = $wdq_internal_url . "?q=" . urlencode ( "claim[$props] $query2" ) ;
$json = json_decode ( file_get_contents ( $url ) ) ;

$hadthat = array() ;

$fp = fopen('clusters.tab', 'w');

foreach ( $json->items AS $x => $q ) {
	if ( isset($hadthat[$q]) ) continue ;
#	print "Checking $q\n" ;
	
	$url = $wdq_internal_url . "?q=" . urlencode ( "web[$q][$props]" ) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	
	$s = count ( $j->items ) ;
#	print "Found a cluster of $s items\n" ;
	fwrite ( $fp , "Q$q\t$s\n" ) ;
	
	foreach ( $j->items AS $i ) $hadthat[$i] = 1 ;
	
//	if ( $x > 10 ) break ;
}

fclose ( $fp ) ;

?>