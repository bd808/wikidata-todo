#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$tfc = new ToolforgeCommon ( 'wikispecies' ) ;
$db = $tfc->openDB ( 'wikidata' , 'wikidata' ) ;
$dbsw = $tfc->openDB ( 'en' , 'wikispecies' ) ;

$qs = $tfc->getQS('wikidata-todo:wikispecies-institutions','/data/project/wikidata-todo/reinheitsgebot.conf') ;

$sql = "SELECT * FROM page WHERE page_id NOT IN (SELECT pp_page FROM page_props WHERE pp_propname='wikibase_item') AND page_namespace=0 AND page_title RLIKE '^[A-Z-]+$' AND page_is_redirect=0" ;
$result = $tfc->getSQL ( $dbsw , $sql ) ;
while($o = $result->fetch_object()){
	$url = "https://species.wikimedia.org/wiki/{$o->page_title}?action=raw" ;
	$wiki = trim ( file_get_contents ( $url ) ) ;
	$wiki_lines = explode ( "\n" , $wiki ) ;
	$wiki_oneline = preg_replace ( '/\s+/' , ' ' , $wiki ) ;

	$cound = false ;
	$commands = [
		'CREATE' ,
		"LAST\tSspecieswiki\t\"{$o->page_title}\""
	] ;
	if ( preg_match ( '/^\s*\{\{[Tt]i\s*\|\s*[A-Z]+\s*\}\}\s*(.+)$/' , $wiki_lines[0] , $m ) ) {
		$label = trim($m[1]) ;
		$label = preg_replace ( "/\s*'{2,3}.*$/" , '' , $label ) ;
		$label = preg_replace ( "/[ ,]+$/" , '' , $label ) ;
		if ( $label == '' ) $label = $o->page_title ;
		$commands[] = "LAST\tP31\tQ43229" ;
		$commands[] = "LAST\tLen\t\"{$label}\"" ;
		$found = true ;
	} else if ( preg_match ( '/\{\{\s*Repository\s*\|\s*name\s*=\s*(.+?)\s*\|/' , $wiki_oneline , $m ) ) {
		$label = trim($m[1]) ;
		if ( $label == '' ) $label = $o->page_title ;
		$commands[] = "LAST\tP31\tQ43229" ;
		$commands[] = "LAST\tLen\t\"{$label}\"" ;
		$found = true ;
	}
	if ( !$found ) continue ;

	$tfc->runCommandsQS ( $commands ) ;
}

?>