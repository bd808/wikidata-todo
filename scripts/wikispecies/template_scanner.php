#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$db = openDB ( 'wikispecies' , 'wikispecies' , true ) ;

$fh = fopen ( './template_includes_template.tab' , 'w' ) ;
//$sql = "select page_title,tl_title from page,templatelinks WHERE page_id=tl_from and page_namespace=10 and tl_namespace=10 AND tl_from_namespace=10 and length(tl_title)>1" ;
$sql = "SELECT * FROM page WHERE page_namespace=10" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'." 2\n$sql\n\n");
while($o = $result->fetch_object()) {
	$url = "https://species.wikimedia.org/w/index.php?action=raw&title=Template:".myurlencode($o->page_title) ;
//	print "{$o->page_title}\n$url\n" ;
	
	$w = file_get_contents ( $url ) ;
	$w = preg_replace ( '/\s+/' , ' ' , $w ) ;
	if ( !preg_match_all ( '/\{\{(.+?)\}\}/' , $w , $m ) ) ;
	if ( count($m[1]) == 0 ) continue ;
	
	foreach ( $m[1] AS $t ) {
		$t = preg_replace ( '/\|.*$/' , '' , $t ) ;
		if ( preg_match ( '/^PAGENAME/' , $t ) ) continue ;
		fwrite ( $fh , $o->page_title . "\t$t\n" ) ;
	}
	
//	print $o->page_title . "\n" ;
//	print_r ( $m ) ;
	
//	print "{$o->page_title}\n$w\n\n\n" ;
}
fclose ( $fh ) ;


?>