#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

//$j = json_decode ( file_get_contents ( 'taxa.json' ) ) ;
#$j = json_decode ( file_get_contents ( 'gena.json' ) ) ;

$hadthat = array() ;

$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$sql = "SELECT ips_site_page FROM wb_items_per_site WHERE ips_site_id='specieswiki'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$hadthat[$o->ips_site_page] = 1 ;
}

//print "in WD: " . count($hadthat) . "\n" ;

$bad_ones = array() ;
$inws = array() ;
$db = openDB ( 'en' , 'wikispecies' , true ) ;
/*
foreach ( array ( 'Disambiguation pages' , 'Hemihomonyms' , 'Repositories' , 'Synonyms' , 'Nomenclatural problems' , 'Biologists' ) AS $cat ) {
	$nono = getPagesInCategory ( $db , $cat , 10 , 0 , true ) ;
	foreach ( $nono AS $n ) {
		$n = str_replace ( '_' , ' ' , $n ) ;
		$hadthat[$n] = $n ;
	}
}
*/

/*
#$sql = "select distinct page_title from page,templatelinks where tl_from=page_id and page_namespace=0 and tl_title IN ('Animalia','Plantae','Fungi','Protista','Eukaryota','Virus','Bacteria','Archaea','Taxonav') AND page_is_redirect=0" ;
$sql = "SELECT page_title FROM page WHERE page_namespace=0 AND page_is_redirect=0" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$t = str_replace ( '_' , ' ' , $o->page_title ) ;
	$t = preg_replace ( '/ \(.+?\) /' , ' ' , $t ) ;
	$t = preg_replace ( '/\s+/' , ' ' , $t ) ;
	if ( isset($hadthat[$t]) ) continue ;
	if ( isset($inws[$t]) ) { // More than one species with that name
		$bad_ones[$t] = 1 ;
		continue ;
	}
	$inws[$t] = 1 ;
}
*/

$lost = explode ( "\n" , file_get_contents ( 'lost_taxa.tab' ) ) ;
foreach ( $lost AS $t ) {
	if ( isset($hadthat[$t]) ) continue ;
	$inws[$t] = 1 ;
}


$db = openDB ( 'wikidata' , 'wikidata' , true ) ;

$fh = fopen ( "new_taxa.add" , 'w' ) ;
$fh_replace = fopen ( "new_taxa.replace" , 'w' ) ;

#$inws = array ( 'Hyposmocoma sabulella' => 1 ) ;

foreach ( $inws AS $taxon => $dummy ) {

#	if ( preg_match ( '/\(/' , $taxon ) ) continue ; // Those are ... odd
#	if ( preg_match ( '/Unspecified/i' , $taxon ) ) continue ; // Those are ... odd

	$q = '' ;
	if ( 1 ) {
		$query = "string[225:\"$taxon\"]" ;
		$url = "http://wdq.wmflabs.org/api?q=" . urlencode ( $query ) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		if ( count($j->items) != 1 ) continue ;
		$q = $j->items[0] ;
	} else {
		$qs = array() ;
		$sql = "SELECT DISTINCT term_entity_id FROM wb_terms WHERE term_entity_type='item' AND term_type IN ('label','alias') AND term_text='" . $db->real_escape_string ( $taxon ) . "'" ;
		$sql .= " AND EXISTS (select * from wb_entity_per_page,pagelinks  WHERE epp_entity_id=term_entity_id AND pl_from=epp_page_id AND epp_entity_type='item' AND pl_from_namespace=0 AND pl_namespace=0 AND pl_title='Q16521')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()) $qs[] = $o->term_entity_id ;
/*		if ( count($qs) == 0 ) {
			$s = "CREATE\n" ;
			$s .= "LAST\tP31\tQ16521\n" ;
			$s .= "LAST\tSspecieswiki\t\"$taxon\"\n" ;
			$s .= "LAST\tLen\t\"$taxon\"\n" ;
			$s .= "LAST\tP225\t\"$taxon\"\n" ;
			fwrite ( $fh , $s ) ;
		}*/
		if ( count($qs) != 1 ) continue ;
		$q = $qs[0] ;
	}

	$skip = false ;
	$sql = "SELECT * FROM wb_items_per_site WHERE ips_item_id=$q AND ips_site_id='specieswiki'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $skip = true ;

	$s = "Q$q\tSspecieswiki\t\"$taxon\"\n" ;
	if ( $skip ) {
		fwrite ( $fh_replace , $s ) ;
	} else {
		fwrite ( $fh , $s ) ;
	}
}

fclose ( $fh ) ;
fclose ( $fh_replace ) ;

?>