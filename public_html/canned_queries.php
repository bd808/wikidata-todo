<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

include_once ( "php/common.php") ;

header('Content-type: application/json');

$callback = get_request ( 'callback' , '' ) ;
$items = get_request ( 'items' , '' ) ;
$params = get_request ( 'params' , '{}' ) ;

$o = array ( 'items' => array() , 'status' => array ( 'error' => 'OK' ) ) ;

$params = json_decode ( $params ) ;
$items = explode ( ',' , $items ) ;


$db = openDB ( 'wikidata' , '' ) ;


if ( isset ( $params->skip_if_proplink ) ) {

	$itemstring = array() ;
	foreach ( $items AS $i ) $itemstring[] = 'Q' . get_db_safe ( $i ) ;
	$itemstring = implode ( "','" , $itemstring ) ;
	
	$propstring = array() ;
	foreach ( $params->skip_if_proplink AS $p ) $propstring[] = 'P' . get_db_safe ( $p ) ;
	$propstring = implode ( "','" , $propstring ) ;

	$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_title IN ('$itemstring') AND NOT EXISTS ( SELECT * FROM pagelinks WHERE pl_from=page_id and pl_namespace=120 and pl_title IN ('$propstring') limit 1)" ;
	$o['sql'] = $sql ;
	
	$result = getSQL ( $db , $sql ) ;
	while($r = $result->fetch_object()){
		$q = substr ( $r->page_title , 1 ) ;
		$o['items'][] = $q * 1 ;
	}
	
}


print json_encode ( $o ) ;

?>