<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('display_errors', 'On');

include_once ( "php/common.php") ;

$script = '' ;

$show_reverse = isset($_REQUEST['reverse']) ;

function print_table2 ( $header , $key , $titles ) {
	global $data , $script , $show_reverse ;
	$tabid = strtolower ( str_replace ( ' ' , '_' , $header ) ) ;
	print "<h2>$header</h2>" ;
	
	begin_tab ( $tabid ) ;

	$plotdata = array() ;
	print "<table class='table table-condensed table-striped'>" ;
	print "<thead><tr><th>Date</th>" ;
	foreach ( $titles AS $k => $v ) {
		$label = ($v=='wikibase-entityid'?'itemlink':$v) ;
		print "<th>$label</th>" ;
		$plotdata[$k*1] = array('data'=>array(),'label'=>$label) ;
	}
	print "<th>Total</th></tr></thead><tbody>" ;
	$datecnt = 0 ;
	$ticks = array() ;
	$last_year = '' ;
	foreach ( $data AS $date => $d ) {
		$pretty_date = substr($date,0,4) . "-" . substr($date,4,2) . "-" . substr($date,6,2) ;
		$cur_year = substr($date,0,4) ;
		$tick = ( $cur_year == $last_year ) ? substr($pretty_date,5,2) : '<b>' . substr($pretty_date,2,5) . '</b>' ;
		$tick = preg_replace ( '/^0+/' , '' , $tick ) ;
		$last_year = $cur_year ;
		$ticks[] = array ( $datecnt , $tick ) ;
		$total = 0 ;
		print "<tr>" ;
		print "<th>" . $pretty_date . "</th>" ;
		foreach ( $titles AS $k => $v ) {
			$n = $d->$key->$v ;
			if ( !isset($n) ) $n = 0 ;
			if ( is_array ( $n ) ) $n = $n[0] ;
			$d->$key->$v = $n ;
			$total += $n ;
			print "<td>" . number_format ( $n ) . "</td>" ;
		}
		print "<td>" . number_format($total) . "</td>" ;
		print "</tr>" ;
		foreach ( $titles AS $k => $v ) {
			$n = $d->$key->$v ;
			$plotdata[$k]['data'][] = array ( $datecnt , $n*1 ) ;
		}
		$datecnt++ ;
	}
	if ( $show_reverse ) $plotdata = array_reverse ( $plotdata ) ;
	print "</tbody></table>" ;
	
	end_tab ( $tabid , $plotdata , $ticks ) ;
}

function begin_tab ( $tabid ) {
	print "<ul class='nav nav-tabs' id='$tabid'>" ;
	print "<li class='nav-item'><a class='nav-link' href='#t1_$tabid'>Table</a></li>" ;
	print "<li class='nav-item'><a class='nav-link active' href='#t2_$tabid'>Plot</a></li></ul>" ;	
	print "<div class='tab-content'><div class='tab-pane' id='t1_$tabid'>" ;
}

function end_tab ( $tabid , $plotdata , $ticks ) {
	global $script ;
	print "</div><div class='tab-pane active' id='t2_$tabid' style='padding:20px'>" ;
	print "<div id='plot_$tabid' style='width:1200px;height:500px'></div>" ;
	print "</div>" ;
	print "</div>" ;
	$script .= "\$('#$tabid a:last').tab('show');\n" ;
	$script .= "\$('#$tabid a').click(function (e) {e.preventDefault();\$(this).tab('show');})\n" ;
	$script .= "plotStack('plot_$tabid'," . json_encode($plotdata) . "," . json_encode($ticks) . ");\n" ;
}

//____________________________________________________________________________________________________

$titles = array ( "itemlink","string","globecoordinate","time","quantity","somevalue","novalue" ) ;

$dir = "/data/project/wikidata-todo/stats/data" ;
$files = scandir ( $dir ) ;

$data = array() ;
foreach ( $files AS $f ) {
	if ( !preg_match ( '/^(.+)\.json$/' , $f , $m ) ) continue ;
	if ( filesize ( "$dir/$f" ) == 0 ) continue ;
	$date = $m[1] ;
	$data[$date] = json_decode ( file_get_contents ( "$dir/$f" ) ) ;
}

$action = get_request ( 'action' , '' ) ;

if ( $action == 'download' ) {
	header('Content-type: application/json');
	print json_encode ( $data ) ;
	exit ( 0 ) ;
}


$s = get_common_header ( '' , "Wikidata Stats" , array (
	'style' => "td { text-align:right !important; } th { text-align:center !important; }" ,
	'link' => '<script src="resources/js/flot/jquery.flot.min.js"></script>' .
				'<script src="resources/js/flot/jquery.flot.stack.min.js"></script>'
) ) ;

#$s = preg_replace ( '/id="main_content"  class="container"/g' , 'id="main_content"  class="container-fluid"' , $s ) ;

print $s ;

print "<div>
These statistics are based on bi-weekly Wikimedia snapshots. <i>Note</i>: Statements can be referenced to both Wikipedia and other sources.<br/>
</div>" ;

if ( $show_reverse ) print "<div>Show <a href='?'>normal stacks</a></div>" ;
else print "<div>Show <a href='?reverse'>reverse stacks</a></div>" ;




print "<h2>Overview</h2>" ;

$tabid = 'overview' ;
$plotdata = array(
	0 => array('data'=>array(),'label'=>'Unreferenced statements') ,
	1 => array('data'=>array(),'label'=>'Statements referenced to Wikipedia') ,
	2 => array('data'=>array(),'label'=>'Statements referenced to other sources')
) ;
$ticks = array() ;
$datecnt = 0 ;
begin_tab ( $tabid ) ;

$last_year = '' ;
print "<table class='table table-condensed table-striped'>" ;
print "<thead><tr><th>Date</th><th>Total Items</th><th>Items with referenced statements</th><th>Total statements</th><th>Statements referenced to Wikipedia</th><th>Statements referenced to other sources</th></tr></thead><tbody>" ;
foreach ( $data AS $date => $d ) {
	$total_statements = 0 ;
	foreach ( $d->statements_per_item AS $k => $v ) $total_statements += $k*$v ;
	$wp_statements = 0 ;
	foreach ( $titles AS $k ) { $n=$d->wp_reference_statements->$k ; $wp_statements += isset($n)?$n:0 ; }
	$non_wp_statements = 0 ;
	foreach ( $titles AS $k ) { $n=$d->non_wp_reference_statements->$k ; $non_wp_statements += isset($n)?$n:0 ; }
	$pretty_date = substr($date,0,4) . "-" . substr($date,4,2) . "-" . substr($date,6,2) ;
	
	$unreferenced_statements = $total_statements - $wp_statements - $non_wp_statements ;
	$plotdata[0]['data'][] = array ( $datecnt , $unreferenced_statements ) ;
	$plotdata[1]['data'][] = array ( $datecnt , $wp_statements ) ;
	$plotdata[2]['data'][] = array ( $datecnt , $non_wp_statements ) ;
	$cur_year = substr($date,0,4) ;
	$tick = ( $cur_year == $last_year ) ? substr($pretty_date,5,2) : '<b>' . substr($pretty_date,2,5) . '</b>' ;
	$tick = preg_replace ( '/^0+/' , '' , $tick ) ;
	$last_year = $cur_year ;
	$ticks[] = array ( $datecnt , $tick ) ;
	$datecnt++ ;

	print "<tr>" ;
	print "<th>" . $pretty_date . "</th>" ;
	print "<td>" . number_format ( $d->total_items ) . "</td>" ;
	print "<td>" . number_format ( $d->items_with_referenced_statements ) . " (" . number_format ( 100*$d->items_with_referenced_statements/$d->total_items , 2 ) . "%)</td>" ;
	print "<td>" . number_format ( $total_statements ) . "</td>" ;
	print "<td>" . number_format ( $wp_statements ) . " (" . number_format ( 100*$wp_statements/$total_statements , 2 ) . "%)</td>" ;
	print "<td>" . number_format ( $non_wp_statements ) . " (" . number_format ( 100*$non_wp_statements/$total_statements , 2 ) . "%)</td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;
end_tab ( $tabid , $plotdata , $ticks ) ;


print_table2 ( 'Referenced statements by statement type' , 'references_per_statement_type' , $titles ) ;
print_table2 ( 'Statements referenced to Wikipedia by statement type' , 'wp_reference_statements' , $titles ) ;
print_table2 ( 'Statements referenced to other sources by statement type' , 'non_wp_reference_statements' , $titles ) ;
print_table2 ( 'References by type' , 'references_properties' , array('wikibase-entityid','string','globecoordinate','time','unknown') ) ;

print_table2 ( 'Statement ranks' , 'rank_per_statement' , array('deprecated','normal','preferred') ) ;
//print_table2 ( 'Statement ranks' , 'rank_per_statement' , array('0','2') ) ;

$max_statement = 10 ;
print "<h2>Statements per item</h2>" ;

$tabid = 'statements' ;
$plotdata = array();
$ticks = array() ;
$datecnt = 0 ;
begin_tab ( $tabid ) ;

$last_year = '' ;
print "<table class='table table-condensed table-striped'>" ;
print "<thead><tr><th>Date</th>" ;
for ( $i = 0 ; $i <= $max_statement ; $i++ ) {
	$label = ($i==$max_statement?'&ge;':'') . $i ;
	print "<th>$label</th>" ;
	$plotdata[$i] = array ( 'data' => array() , 'label' => "$label statements" ) ;
}
print "</tr></thead><tbody>" ;
foreach ( $data AS $date => $d ) {
	$cnt = array() ;
	$pretty_date = substr($date,0,4) . "-" . substr($date,4,2) . "-" . substr($date,6,2) ;
	$cur_year = substr($date,0,4) ;
	$tick = ( $cur_year == $last_year ) ? substr($pretty_date,5,2) : '<b>' . substr($pretty_date,2,5) . '</b>' ;
	$tick = preg_replace ( '/^0+/' , '' , $tick ) ;
	$last_year = $cur_year ;
	$ticks[] = array ( $datecnt , $tick ) ;
	foreach ( $d->statements_per_item AS $k => $v ) {
		if ( $k > $max_statement ) $cnt[$max_statement] += $v ;
		else $cnt[$k] += $v ;
	}
	print "<tr>" ;
	print "<th>" . $pretty_date . "</th>" ;
	for ( $i = 0 ; $i <= $max_statement ; $i++ ) {
		$v = isset ( $cnt[$i] ) ? $cnt[$i] : 0 ;
		print "<td>" . number_format($v) . "<br/><small>(" . number_format($v*100/$d->total_items,2) . "%)</small></td>" ;
		$plotdata[$i]['data'][] = array ( $datecnt , $v ) ;
	}
	print "</tr>" ;
	$datecnt++ ;
}
if ( $show_reverse ) $plotdata = array_reverse ( $plotdata ) ;
print "</tbody></table>" ;
end_tab ( $tabid , $plotdata , $ticks ) ;


if ( !isset ( $d->descs_per_item ) ) $d->descs_per_item = array() ;

$modes = array ( 'labels' => "Labels" , 'descs' => "Descriptions" , 'links' => 'Wiki(m|p)edia links' ) ;
foreach ( $modes AS $mode_key => $mode_title ) {
	$max_cnt = 10 ;
	print "<h2>$mode_title per item</h2>" ;

	$tabid = $mode_key ;
	$plotdata = array();
	$ticks = array() ;
	$datecnt = 0 ;
	begin_tab ( $tabid ) ;

	print "<table class='table table-condensed table-striped'>" ;
	print "<thead><tr><th>Date</th>" ;
	for ( $i = 0 ; $i <= $max_cnt ; $i++ ) {
		$label = ($i==$max_cnt?'&ge;':'') . $i ;
		print "<th>$label</th>" ;
		$plotdata[$i] = array ( 'data' => array() , 'label' => "$label $mode_key" ) ;
	}
	print "</tr></thead><tbody>" ;

	$last_year = '' ;	
	foreach ( $data AS $date => $d ) {

		$cnt = array() ;
		$pretty_date = substr($date,0,4) . "-" . substr($date,4,2) . "-" . substr($date,6,2) ;
		$cur_year = substr($date,0,4) ;
		$tick = ( $cur_year == $last_year ) ? substr($pretty_date,5,2) : '<b>' . substr($pretty_date,2,5) . '</b>' ;
		$tick = preg_replace ( '/^0+/' , '' , $tick ) ;
		$last_year = $cur_year ;
		$ticks[] = array ( $datecnt , $tick ) ;
		$d2 = $d->labels_per_item ;
		if ( $mode_key == 'descs' ) $d2 = $d->descs_per_item ;
		if ( $mode_key == 'links' ) $d2 = $d->links_per_item ;
		if ( !isset ( $d2 ) ) $da = array() ; // Paranoia
		foreach ( $d2 AS $k => $v ) {
			if ( $k > $max_cnt ) $cnt[$max_cnt] += $v ;
			else $cnt[$k] += $v ;
		}
		print "<tr>" ;
		print "<th>" . $pretty_date . "</th>" ;
		for ( $i = 0 ; $i <= $max_cnt ; $i++ ) {
			$v = isset ( $cnt[$i] ) ? $cnt[$i] : 0 ;
			print "<td>" . number_format($v) . "</td>" ;
			$plotdata[$i]['data'][] = array ( $datecnt , $v ) ;
		}
		print "</tr>" ;
		$datecnt++ ;
	}
	if ( $show_reverse ) $plotdata = array_reverse ( $plotdata ) ;
	print "</tbody></table>" ;
	end_tab ( $tabid , $plotdata , $ticks ) ;
}


//____________________________________________________________________________________________________


print "<h2>Download stats files</h2>" ;
print "<div><a href='?action=download'>Download above data</a> as JSON.</div>" ;

$script .= '$("div.container").removeClass("container").addClass("container-fluid");' ;
$script .= '$("div.row").removeClass("row").addClass("row-fluid");' ;

print '<script>

	function plotStack ( id , data , ticks ) {
		$.plot("#"+id, data , {
				series: {
					stack: true,
					lines: {
						show: false,
						fill: true,
						steps: false
					},
					bars: {
						show: true,
						hoverable: true,
						barWidth: 0.6
					}
				} ,
				legend : {
					show:true,
					position:"nw",
					margin:10,
					backgroundColor:"white",
					backgroundOpacity:1
				} ,
				xaxis : {
					show:true,
					axisLabelFontSizePixels: 7,
					ticks:ticks
				} ,
				yaxis : {
					show:true,
					position:"right",
					tickFormatter:function(value,axis){return value==0?"":(Math.floor(value/100000)/10)+"M"}
				}
			});
		var x = $("#"+id+" div.legend tbody") ;
		x.children().each(function(i,tr){x.prepend(tr)});
	}

$(document).ready(function(){' . $script . '})
</script>' ;

print get_common_footer() ;

?>