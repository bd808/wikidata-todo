<?PHP

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
include_once ( 'php/wikiquery.php' ) ;

$tsf = "D, d M Y H:i:s " ;
$tstz = "GMT" ;
$ts = date ( $tsf ) . $tstz  ;

$wdq = trim ( get_request ( 'wdq' , '' ) ) ;
$sparql = trim ( get_request ( 'sparql' , '' ) ) ;
$max = 1 * get_request ( 'max' , '50' ) ;
$thumbsize = 1 * get_request ( 'thumbsize' , '300' ) ;
$demo_sparql = "SELECT ?item ?image WHERE {\n?item wdt:P31 ?sub0 .\n?sub0 (wdt:P279)* wd:Q12280 .\n?item wdt:P177 wd:Q1653 .\n?item wdt:P18 ?image\n}" ;

if ( !isset($_REQUEST['doit']) or $wdq.$sparql == '' ) {
	print get_common_header ( '' , 'WDQ image feed' ) ;
	print "<div class='lead'>Generate an image RSS feed (for your screensaver?) from a <a href='//query.wikidata.org'>SPARQL</a> or <a href='//wdq.wmflabs.org'>WDQ</a> query!</div>
	<form method='get' class='form form-inline inline-form' action='?'>
	<textarea name='sparql' rows=3 style='width:100%' placeholder='Type SPARQL query here! (First variable item, second variable image)'></textarea>
	<i>or</i> <input type='text' name='wdq' size='60' placeholder='Type WDQ query here!' value='$wdq'/>
	<input type='submit' name='doit' value='Generate feed' class='btn btn-primary' />
	<br/><input size='10' type='number' name='thumbsize' value='$thumbsize' />px thumb size
	</form>" ;
	print "<div>Of all returned items with images, a random subset of 50 will be used.</div>" ;
	print "<div>Example: <a href='?sparql=".urlencode($demo_sparql)."&doit=Generate+feed'>Bridges across the Danube</a> (<a href='https://query.wikidata.org/#".($demo_sparql)."' target='_blank'>edit query</a>)</div>" ;
	exit ( 0 ) ;
}

function xmlify ( $text ) {
	$doc = new DOMDocument();
	$fragment = $doc->createDocumentFragment();
	$fragment->appendChild($doc->createTextNode($text));
	return $doc->saveXML($fragment);
}

function get_thumb ( $image ) {
	global $thumbsize ;
	$wq = new WikiQuery ( 'commons' , 'wikimedia' ) ;
	$ret = $wq->get_image_data ( "File:$image" , round ( $thumbsize ) ) ;
	$ret = $ret['imageinfo'][0]['thumburl'] ;
	return $ret ;
}



$params = array() ;

if ( $wdq != '' ) $params[] = "wdq=" . urlencode ( $wdq ) ;
if ( $sparql != '' ) $params[] = "sparql=" . urlencode ( $sparql ) ;
$params[] = "doit=1" ;


$eighteen = '18' ;
$a = array() ;

if ( $sparql == '' ) {
	$j = json_decode ( file_get_contents ( "$wdq_internal_url?q=".urlencode("($wdq) AND CLAIM[18]")."&props=18") ) ;
	if ( isset($j->props) and isset($j->props->$eighteen) ) $a = (array) $j->props->$eighteen ;
} else {
	$j = getSPARQL ( $sparql ) ;
	$item = $j->head->vars[0] ;
	$image = $j->head->vars[1] ;
	foreach ( $j->results->bindings AS $b ) {
		$q = preg_replace ( '/^.+entity\/Q/' , '' , $b->$item->value ) ;
		$file = urldecode ( preg_replace ( '/^.+Special:FilePath\//' , '' , $b->$image->value ) ) ;
		$a[] = array ( $q , '' , $file ) ;
	}
//		header('Content-Type: text/plain');		print_r ( $a ) ;		exit(0);
}

if ( 1 ) header('Content-type: application/rss+xml; charset=utf-8');
else header('Content-Type: text/plain'); 

print '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>

<atom:link href="//tools.wmflabs.org/wikidata-todo/wdq_image_feed.php?' . xmlify(implode('&',$params)) . '" rel="self" type="application/rss+xml" /><title>' . xmlify($wdq) . ' - www.wikidata.org</title>
<link>http://tools.wmflabs.org/catfood/catfood.php</link><description>Query ' . xmlify($wdq) . ' for www.wikidata.org</description><language>en-us</language>
<copyright>Feed: CC-0; Images: see description page</copyright><pubDate>Thu, 19 Mar 2015 00:46:21 GMT</pubDate><lastBuildDate>Thu, 19 Mar 2015 00:46:21 GMT</lastBuildDate><generator>CatFood</generator>
<docs>http://tools.wmflabs.org/wikidata-todo/wdq_image_feed.php</docs>


<image>
<url>//upload.wikimedia.org/wikipedia/commons/thumb/6/66/Wikidata-logo-en.svg/200px-Wikidata-logo-en.svg.png</url>
<title>' . xmlify(implode('&',$params)) . '</title>
<link>http://tools.wmflabs.org/wikidata-todo/wdq_image_feed.php</link>
</image>' ;

shuffle ( $a ) ;
$images = array_slice ( $a , 0 , $max ) ;

$db = openDB ( 'commons' , "wikimedia" ) ;
$sql = array() ;
foreach ( $images AS $v ) {
	$v[2] = str_replace(' ','_',$v[2]) ;
	$sql[] = $db->real_escape_string ( $v[2] ) ;
}
$sql = "SELECT * FROM image WHERE img_name IN ('" . implode("','",$sql) . "')" ;
$result = getSQL ( $db , $sql ) ;
$meta = array() ;
while($o = $result->fetch_object()){
	$meta[$o->img_name] = $o ;
}



foreach ( $images AS $v ) {
	$v[2] = str_replace(' ','_',$v[2]) ;
	$q = $v[0] ;
	$i = $v[2] ;
	$iurl = xmlify('http://commons.wikimedia.org/wiki/File:'.myurlencode($i)) ;
	$turl = xmlify ( get_thumb ( $i ) ) ;
	$user = $meta[$i]->img_user_text ;
print '<item>
<title>' . xmlify(str_replace('_',' ',$i)) . '</title>
<pubDate>' . $ts . '</pubDate>
<link>' . $iurl . '</link>
<guid isPermaLink="false">' . $iurl . ' - ' . $ts . '</guid>
<description>
&lt;a href="' . $iurl . '"&gt;&lt;img border="0" src="' . $turl . '" /&gt;&lt;/a&gt;&lt;br/&gt;' ;

	if ( $user != '' ) {
		print 'Uploaded by &lt;a href="//commons.wikimedia.org/wiki/User:'.myurlencode($user).'"&gt;'.xmlify($user).'&lt;/a&gt;&lt;br/&gt;' ;
	}
	print 'Used by Wikidata item &lt;a href="//www.wikidata.org/wiki/Q'.$q.'"&gt;Q'.$q.'&lt;/a&gt;&lt;br/&gt;' ;
	print '</description></item>' ;
}

// https://tools.wmflabs.org/catfood/catfood.php?language=commons&project=wikimedia&category=Varanus+komodoensis&depth=0&namespace=6&user=&size=300&last=10&doit=Do+it


print '</channel></rss>' ;

?>