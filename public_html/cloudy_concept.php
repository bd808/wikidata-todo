<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');
//ini_set('memory_limit','500M');
set_time_limit ( 60 * 15 ) ; // Seconds

require_once ( 'php/common.php' ) ;

$db = openDB ( 'wikidata' , '' ) ;

$testing = get_request ( 'testing' , false ) ;
$q = preg_replace ( '/\D/' , '' , get_request ( 'q' , '' ) ) ;
$lang = get_request ( 'lang' , substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) ) ;
$lang = $db->real_escape_string ( $lang ) ;

print get_common_header ( '' , 'Concept cloud' ) ;

print '
<script type="text/javascript" src="./resources/js/jquery/jquery.cluetip.min.js"></script>
<script type="text/javascript" src="./resources/js/wikidata.js"></script>
<script type="text/javascript" src="./autodesc.js"></script>
<script type="text/javascript" src="./cloudy_concept.js"></script>

<style>
/* Cluetip CSS */
h3.ui-cluetip-header {
	line-height: 100%;
	font-size:1.5em;
	padding:2px;
	padding-left:10px;
}

.cluetip-inner div {
	margin-bottom:3px;
}
</style>
' ;


print "<div class='well'>This tool looks for unlabeled Wikidata items in your language. It starts with a Wikidata item, 
looks at all the associated Wikipedia pages, the links on all those pages, and lists the Wikidata items corresponding to the link targets.</div>
<form class='form-inline' method='get'>
<table class='table table-condensed table-striped'>
<tr><th nowrap>Wikidata item</th><td style='width:100%'><div class='input-prepend'><span class='add-on'>Q</span><input name='q' value='$q' type='text' /></div> 
(<a href='?q=44461&lang=$lang'>Example</a> | <a href='?q=Q4026244&lang=$lang'>In the news</a>)</td></tr>
<tr><th nowrap>Language code</th><td><input name='lang' value='$lang' type='text' /></td></tr>
<tr><td/><td><input type='submit' value='Cloud it!' class='btn btn-outline-primary' /></td></tr>
</table>
</form>" ;


$icon = 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Weather-sun-unsettled.svg/40px-Weather-sun-unsettled.svg.png' ;
print "<script>$(document).ready(function(){\$('<a href=\"//commons.wikimedia.org/wiki/File:Weather-sun-unsettled.svg\" title=\"Weather-sun-unsettled.svg\"><img border=0 src=\"$icon\" /></a>').insertAfter('#toolname');});</script>" ;

if ( $q != '' ) {

	$to_load = array(preg_replace('/\D/','',$q)) ;

	$initial_namespace = 0 ;
	$sql = "select DISTINCT pl_title from pagelinks,page where page_title='Q$q' and page_namespace=0 and pl_from=page_id and pl_namespace=0" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		if ( $o->pl_title == 'Q11266439' ) $initial_namespace = 10 ; // Template
		if ( $o->pl_title == 'Q4663903' ) $initial_namespace = 100 ; // Project
	}

	$label = '' ;
	$sql = "SELECT wbx_text
FROM wbt_item_terms,wbt_term_in_lang,wbt_text_in_lang,wbt_text
WHERE wbit_item_id={$q}
AND wbit_term_in_lang_id=wbtl_id
AND wbtl_type_id=1 # Label
AND wbtl_text_in_lang_id=wbxl_id
AND wbxl_language='{$lang}'
AND wbxl_text_id=wbx_id" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$label = $o->wbx_text ;
	}
	
	print "<h2><a class='q_internal' q='$q' target='_blank' href='//www.wikidata.org/wiki/Q$q'>Q$q</a> - " . ( $label=='' ? "<i style='color:red'>No label in $lang!</i>" : $label ) . "</h2>" ;
	
	$sql = "select * from wb_items_per_site WHERE ips_item_id=$q" ; // $q is already regexp-filtered for digits only!
	$result = getSQL ( $db , $sql ) ;
	$sites = array() ;
	while($o = $result->fetch_object()){
		if ( !preg_match ( '/^(.+)wiki$/' , $o->ips_site_id , $m ) ) continue ; // Wikipedias only
		$sites[$m[1]] = get_db_safe ( $o->ips_site_page ) ;
	}
	
	$num_wikis = count ( $sites ) ;
	print "<hr/><div>Searching link targets on $num_wikis Wikipedias...</div>" ; myflush();
	
	$items = array() ;
	$page_titles = array() ;
	$max_width = 935 ;
	print "<div style='width:{$max_width}px;border-bottom:1px solid black;'></div>" ;
	print "<div style='width:{$max_width}px;border-bottom:1px solid black;padding:0px;margin:0px'>" ;
	foreach ( $sites AS $dblang => $page ) { // $page is already db-safe
		if ( $initial_namespace != 0 ) $page = preg_replace ( '/^[^:]+:/' , '' , $page ) ;
//		print "<span class='badge badge-success' style='margin-right:2px;'>$dblang</span>" ; myflush();
		print "<div style='display:inline-block;height:10px;background-color:green;width:" . ($max_width/$num_wikis) . "px'></div>" ; myflush() ;
		$db2 = openDB ( $dblang , 'wikipedia' ) ;
		if ( false === $db2 ) continue ; // Error; ignore
		$sql = "SELECT DISTINCT p2.page_title AS title FROM page p1,page p2,pagelinks WHERE pl_from=p1.page_id AND p1.page_namespace=$initial_namespace AND p1.page_title='$page' AND pl_namespace=0 AND p2.page_namespace=0 AND p2.page_title=pl_title" ;
		$result = getSQL ( $db2 , $sql ) ;
		$targets = array() ;
		while($o = $result->fetch_object()){
			$targets[] = $db2->real_escape_string ( str_replace ( '_' , ' ' , $o->title ) ) ;
		}
		$db2->close() ;
		
		if ( count ( $targets ) == 0 ) continue ; // My work here is done
		$sql = "SELECT ips_item_id,ips_site_page FROM wb_items_per_site WHERE ips_site_id='".$dblang."wiki' AND ips_site_page IN ('".implode("','",$targets)."')" ;
		$result = getSQL ( $db , $sql ) ;
		while($o = $result->fetch_object()){
			$items[$o->ips_item_id]++ ;
			$wiki_title = str_replace ( ' ' , '&nbsp;' , $o->ips_site_page ) ;
			$page_titles[$o->ips_item_id][$wiki_title]++ ;
		}
	}
	print "</div>" ;
//	if ( $testing ) print "<pre>" ; print_r ( $items ) ; print "</pre>" ;

	if ( count ( $items ) > 0 ) {
		arsort ( $items , SORT_NUMERIC ) ;
	
		$labels = array() ;
		#$sql = "SELECT term_full_entity_id,term_text FROM wb_terms WHERE term_type='label' AND term_entity_type='item' AND term_language='$lang' AND term_full_entity_id IN ('Q".implode("','Q",array_keys($items))."')" ;
		$sql = "SELECT wbit_item_id,wbx_text
FROM wbt_item_terms,wbt_term_in_lang,wbt_text_in_lang,wbt_text
WHERE wbit_item_id IN (" . implode(",",array_keys($items)) . ")
AND wbit_term_in_lang_id=wbtl_id
AND wbtl_type_id=1 # Label
AND wbtl_text_in_lang_id=wbxl_id
AND wbxl_language='{$lang}'
AND wbxl_text_id=wbx_id" ;
#print "<pre>{$sql}</pre>";
		$result = getSQL ( $db , $sql ) ;
		while($o = $result->fetch_object()){
			$oq = "{$o->wbit_item_id}" ;
			$labels[$oq] = $o->wbx_text ;
		}
	} else {
		print "<hr/><p>No wiki pages are associated with this item</p>" ;
		$db->close() ;
		print get_common_footer() ;
		exit() ;
	}

	print "<br/><div><a href='#' onclick='$(\"tr.exists\").toggle();return false'>Toggle existing labels</a></div>" ;
	print "<div><table class='table table-condensed table-striped'>" ;
	print "<thead><tr><th>Item</th><th>Label&nbsp;[$lang]</th><th>Wikilabels</th><th>Link&nbsp;#</th></tr></thead><tbody>" ;
	foreach ( $items AS $item => $cnt ) {
		$to_load[] = preg_replace('/\D/','',$item) ;
		$labeled = isset ( $labels[$item] ) ;
		print "<tr class='" . ($labeled?"exists":"missing") . "'>" ;
		print "<th nowrap>" ;
		print "<a class='q_internal' q='$item' target='_blank' href='//www.wikidata.org/wiki/Q$item'>Q$item</a>" ;
		print "<small>&nbsp;[<a href='?q=$item&lang=$lang' title='Concept cloud for this item'>CC</a>&nbsp;|&nbsp;" ;
		print "<a href='reasonator.toolforge.org/?q=$item&lang=$lang'><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/16px-Reasonator_logo_proposal.png' border=0 /></a>]</small>" ;
		print "</th>" ;
		print "<td>" ;
		if ( $labeled ) print $labels[$item] ;
		else print "<i style='color:red'>No label in $lang!</i>" ;
		print "</td>" ;
		print "<td style='font-size:8pt'>" ;
		arsort ( $page_titles[$item] , SORT_NUMERIC ) ;
		print implode ( " | " , array_keys ( $page_titles[$item] ) ) ;
		print "</td>" ;
		print "<td style='text-align:right;font-family:Courier'>$cnt</td>" ;
		print "</tr>" ;
	}
	print "</tbody></table></div>" ;
	
	print "<script>to_load=[" . implode(',',$to_load) . "];</script>" ;
	

}


$db->close() ;
print get_common_footer() ;

?>